#!/usr/bin/env bash

cat /usr/share/nginx/html/prebind/index.tmpl | envsubst '${RC_OIDC_METHOD_NAME} ${IDP_HINT_PARAM}' > /usr/share/nginx/html/prebind/index.html

nginx -g "daemon off;"
