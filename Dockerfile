FROM nginxinc/nginx-unprivileged:1.19.2

ENV RC_OIDC_METHOD_NAME ""

USER root

RUN mkdir -p /usr/share/nginx/html/prebind/bower_components
COPY index.html /usr/share/nginx/html/prebind/index.tmpl
COPY bower_components /usr/share/nginx/html/prebind/bower_components
COPY init.sh /

RUN chown 101 /usr/share/nginx/html -R
RUN chmod +x /init.sh

USER 101

CMD ["/init.sh"]
